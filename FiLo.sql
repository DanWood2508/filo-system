-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2020 at 02:39 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `FiLo`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `found_time` time NOT NULL,
  `found_date` date NOT NULL,
  `found_user` bigint(20) UNSIGNED NOT NULL,
  `found_place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `category`, `found_time`, `found_date`, `found_user`, `found_place`, `colour`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Jewellery', '10:16:01', '2020-02-05', 1, 'Winfieldton', 'white', 'Quasi facilis in enim neque occaecati. Non vel reiciendis minus. Dignissimos labore aut doloremque.', '2020-04-27 12:37:29', '2020-04-27 13:26:58'),
(2, 'Jewellery', '11:16:54', '2013-02-09', 11, 'Lake Morton', 'olive', 'Hic nulla nulla maiores in quo ullam aperiam rerum. Laudantium et distinctio delectus nemo.', '2020-04-27 12:37:29', '2020-04-27 13:21:34'),
(3, 'Phone', '13:00:23', '2018-03-24', 5, 'Port Jadenstad', 'silver', 'Quia dolorem voluptatem soluta. Voluptas ut at esse nostrum. Saepe suscipit dolor illum.', '2020-04-27 12:37:29', '2020-04-27 13:21:46'),
(4, 'Jewellery', '09:50:33', '1985-12-25', 8, 'Annettaport', 'silver', 'Et ea fugit deserunt qui recusandae. Totam eos fugiat non ut aut. Debitis voluptas a et vero.', '2020-04-27 12:37:29', '2020-04-27 13:21:57'),
(5, 'Phone', '00:22:26', '1996-11-30', 11, 'New Noreneberg', 'purple', 'Porro occaecati est neque. Officia voluptatibus quod beatae soluta est et.', '2020-04-27 12:37:29', '2020-04-27 13:22:09'),
(6, 'Jewellery', '17:54:10', '2019-03-27', 9, 'Kathlynfort', 'navy', 'Voluptatem nostrum omnis qui. Et aut atque asperiores ad.', '2020-04-27 12:37:29', '2020-04-27 13:22:20'),
(7, 'Phone', '10:19:23', '1987-12-19', 4, 'South Joe', 'white', 'Assumenda aut ullam ad explicabo. Dolor eligendi impedit sit hic officiis corrupti.', '2020-04-27 12:37:29', '2020-04-27 13:22:45'),
(8, 'Pet', '14:42:42', '1973-03-21', 3, 'New Thomas', 'navy', 'Quaerat eius corporis eum at quod dicta vel. Rerum minus dolores et pariatur ut sint.', '2020-04-27 12:37:29', '2020-04-27 13:22:56'),
(9, 'Pet', '11:38:43', '2020-02-26', 2, 'Hoppemouth', 'gray', 'Nam nobis eaque et est. Praesentium totam ut molestias voluptas deleniti et dolore.', '2020-04-27 12:37:29', '2020-04-27 13:23:06'),
(10, 'Phone', '15:20:55', '1981-05-07', 7, 'Lorenzobury', 'silver', 'Aperiam libero itaque ab ad voluptatem voluptatibus. Cumque quo repellat sint odit aut assumenda.', '2020-04-27 12:37:29', '2020-04-27 13:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `item_details`
--

CREATE TABLE `item_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_details`
--

INSERT INTO `item_details` (`id`, `item_id`, `filename`, `created_at`, `updated_at`) VALUES
(5, 1, 'photos/jZimMnJVnk1J8vwfWAv1kSau0Uvi4o5UudyDdUnC.png', '2020-04-27 13:21:24', '2020-04-27 13:21:24'),
(6, 1, 'photos/bUQvvvV1jnNvkrt1tofPPPkAnowxD00mrOgFz6dj.png', '2020-04-27 13:21:24', '2020-04-27 13:21:24'),
(7, 1, 'photos/3BIf9MWerzLwKJ3z1QGInCt02E3q4R7KXgRSDhgp.png', '2020-04-27 13:21:24', '2020-04-27 13:21:24'),
(8, 2, 'photos/zOy3PFWwBph6xZpCNpUafdBbJLBLkNSdiQIcI7hC.jpeg', '2020-04-27 13:21:34', '2020-04-27 13:21:34'),
(9, 2, 'photos/IrRjPMvTUMcnBOOb9MS4M3XEjCtTD19HqDr2rki4.png', '2020-04-27 13:21:34', '2020-04-27 13:21:34'),
(10, 2, 'photos/b7G7iP9NZmFeZXVC9jNB79GwCbRIejeDydkxeJXZ.jpeg', '2020-04-27 13:21:34', '2020-04-27 13:21:34'),
(11, 3, 'photos/NIcdgZiqvzdZuS5lJRoWN6SzgD7RtTgNFveVv8cs.png', '2020-04-27 13:21:46', '2020-04-27 13:21:46'),
(12, 3, 'photos/KuZvNf6PS8zGtyIEml9vqPqscVK7or76ZULzxXXv.png', '2020-04-27 13:21:46', '2020-04-27 13:21:46'),
(13, 4, 'photos/vzSwuJR8IKDaTTOpnRUVxf4Zqtr0P52fNIZlbovc.png', '2020-04-27 13:21:57', '2020-04-27 13:21:57'),
(14, 4, 'photos/lkzBJkmkKkdUlV0d9N6iiNLsJIdXlNrKTt7iUF95.png', '2020-04-27 13:21:57', '2020-04-27 13:21:57'),
(15, 5, 'photos/6FRQisWbnCYxVWmGUC3jtl1WOxYhW7j4BO699sVN.png', '2020-04-27 13:22:09', '2020-04-27 13:22:09'),
(16, 5, 'photos/CE8hHsavbAj1y8tdj184YDlTbgy26dkrimNeIqx9.png', '2020-04-27 13:22:09', '2020-04-27 13:22:09'),
(17, 5, 'photos/ZYD2whDsIyR7qWqHqJTqyL3qd5FExoWu95JTOxDH.jpeg', '2020-04-27 13:22:09', '2020-04-27 13:22:09'),
(18, 6, 'photos/VxXJoK6UAfKNk5XvTzYKHkgO1Qhh3nQrosd0zzrd.png', '2020-04-27 13:22:20', '2020-04-27 13:22:20'),
(19, 7, 'photos/TRBZnaFoqZFx1aGjGO80gi9BvmwVha7HxJVQEscA.png', '2020-04-27 13:22:45', '2020-04-27 13:22:45'),
(20, 8, 'photos/wTboT4tbArYjA2iStvXWy8rhNCkseD2UOZ7UL3dC.jpeg', '2020-04-27 13:22:56', '2020-04-27 13:22:56'),
(21, 8, 'photos/wO39kTo2fwSrQ5Na0YyYoDRFHx8lf4kmj3Sx62rF.jpeg', '2020-04-27 13:22:56', '2020-04-27 13:22:56'),
(22, 9, 'photos/HNYa36RRKbn5vKZzlVE9Sekp3KcIhcnz76JoM6dh.jpeg', '2020-04-27 13:23:06', '2020-04-27 13:23:06'),
(23, 10, 'photos/KcqRDJ4tT8FNK0UUmfI0ULVwPCpCWnmn2uYl22uu.png', '2020-04-27 13:23:21', '2020-04-27 13:23:21'),
(24, 10, 'photos/rTgJXtML9RoJ2XGxx0eqRJMhQLbdTUycKIZmIbvT.png', '2020-04-27 13:23:21', '2020-04-27 13:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_04_15_155503_create_items_table', 1),
(5, '2020_04_15_155537_create_requests_table', 1),
(6, '2020_04_20_150214_create_item_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Submitted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `user_id`, `item_id`, `reason`, `status`, `created_at`, `updated_at`) VALUES
(1, 10, 1, 'Qui saepe qui sequi ipsa. Eligendi accusamus vero fugit perferendis.', 'Submitted', '2020-04-27 12:37:29', NULL),
(2, 9, 5, 'Tenetur ut illo est voluptas ea. Qui laudantium ut sit necessitatibus.', 'Submitted', '2020-04-27 12:37:29', NULL),
(3, 4, 7, 'Expedita laudantium et beatae consequatur id ab. Sed nam voluptatum reprehenderit ratione sit.', 'Submitted', '2020-04-27 12:37:29', NULL),
(4, 9, 5, 'Sit velit natus quia omnis. Veniam quia voluptatum facilis sunt.', 'Submitted', '2020-04-27 12:37:29', NULL),
(5, 1, 7, 'Facere et ullam ducimus ut incidunt pariatur. Libero velit non quae qui.', 'Submitted', '2020-04-27 12:37:29', NULL),
(6, 2, 7, 'Asperiores non eos occaecati vitae et. Quaerat cum autem vero fugit asperiores omnis quia.', 'Submitted', '2020-04-27 12:37:29', NULL),
(7, 8, 4, 'Est deserunt velit dolorem dicta doloribus deleniti ad eos. Vitae dolorem et autem.', 'Submitted', '2020-04-27 12:37:29', NULL),
(8, 7, 8, 'Ullam adipisci veniam itaque explicabo. Et non dolor labore corporis.', 'Submitted', '2020-04-27 12:37:29', NULL),
(9, 5, 1, 'Mollitia repellat commodi est modi. Et voluptatem ipsa accusamus est. Autem ratione omnis aut est.', 'Submitted', '2020-04-27 12:37:29', NULL),
(10, 3, 8, 'Aut et sed iste corporis ut rerum. Quae in unde veniam qui et alias amet necessitatibus.', 'Submitted', '2020-04-27 12:37:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `second_name`, `role`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Ardith', 'Schuster', 0, 'jenifer89@rogahn.com', 'k&e.wc6vX1A`', '2020-04-27 12:37:29', NULL),
(2, 'Mohammad', 'Dooley', 0, 'yazmin83@feest.com', 'js)*Dd=3?KGw', '2020-04-27 12:37:29', NULL),
(3, 'Alysa', 'Schowalter', 0, 'jdickinson@yahoo.com', 'nE9kV%j`s\'42', '2020-04-27 12:37:29', NULL),
(4, 'Enrico', 'Stoltenberg', 0, 'bward@anderson.com', 'fT*_?D14p,[^', '2020-04-27 12:37:29', NULL),
(5, 'Arne', 'O\'Keefe', 0, 'qkeebler@hotmail.com', 'j#CK?LkFMCK@', '2020-04-27 12:37:29', NULL),
(6, 'Veda', 'Rosenbaum', 0, 'hweissnat@hayes.com', '+IjOEBq:B[xM', '2020-04-27 12:37:29', NULL),
(7, 'Gabriel', 'White', 0, 'stephania25@yahoo.com', '\'o)oe]zxgX^U', '2020-04-27 12:37:29', NULL),
(8, 'Everett', 'Ondricka', 0, 'srippin@beahan.info', ';PsEmNz*t6$J', '2020-04-27 12:37:29', NULL),
(9, 'Willa', 'Sanford', 0, 'sage.swift@yahoo.com', 'D@6Rk%s#Ofge', '2020-04-27 12:37:29', NULL),
(10, 'Janessa', 'Torp', 0, 'dejah54@hotmail.com', 'Ld=j*M6.TBZy', '2020-04-27 12:37:29', NULL),
(11, 'Admin', 'User', 1, 'admin@filo.com', '$2y$10$ixdJOJ6gLEVCuHZaTzeVrelMvHV1l72pbmr5ac2BerixHjNG3Lo8i', '2020-04-27 12:38:35', '2020-04-27 12:38:35'),
(12, 'Standard', 'User', 0, 'user@filo.com', '$2y$10$9FlquRVhSM9s03hxwSjaSuse7hdQW6cXnQ0WWcDAVPecvmwm6bVCy', '2020-04-27 12:38:48', '2020-04-27 12:38:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_found_user_foreign` (`found_user`);

--
-- Indexes for table `item_details`
--
ALTER TABLE `item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_details_item_id_foreign` (`item_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requests_user_id_foreign` (`user_id`),
  ADD KEY `requests_item_id_foreign` (`item_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `item_details`
--
ALTER TABLE `item_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_found_user_foreign` FOREIGN KEY (`found_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `item_details`
--
ALTER TABLE `item_details`
  ADD CONSTRAINT `item_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`);

--
-- Constraints for table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `requests_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
