<?php

use Illuminate\Support\Facades\Route;
use App\Mail\RequestMail;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Auth::routes();

Route::get('/home', 'ItemController@list')->name('home');
Route::get('home/show', 'ItemController@show')->name('show');
Route::get('home/create', 'ItemController@create')->name('create');
Route::get('home/create', 'ItemController@create')->name('post');
Route::get('home/requests/', 'RequestController@list')->name('show');
Route::get('home/requests/userdetails', 'UserController@show');
Route::get('home/items/edit', 'ItemController@edit');
Route::post('home/items/delete', 'ItemController@destroy');

Route::get('/multiuploads', 'UploadController@uploadForm');
Route::post('/multiuploads', 'UploadController@uploadSubmit');

Route::get('image-view', 'ImageController@index');
Route::resource('items','ItemController');
Route::resource('users','UserController');
Route::resource('requests', 'RequestController');

Route::get('/email', function() {
  Mail::to('email@email.com')->send(new RequestMail());
  return new RequestMail();
});
