<?php

namespace App\Mail;

use App\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @var item;
     * @var user;
     * @var request;
     */
     protected $item;
     protected $user;
     protected $request;

    public function __construct($item, $user, $request)
    {
        $this->item = $item;
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('emails.requests')
                  ->with([
                    'firstName' => $this->user->first_name,
                    'secondName' => $this->user->second_name,
                    'itemColour' => $this->item->colour,
                    'itemCategory' => $this->item->category,
                    'requestSubmitted' => $this->request->created_at,
                    'status' => $this->request->status
                  ]);
    }
}
