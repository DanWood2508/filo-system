<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Requests extends Model
{
  use Sortable;
  // These are the attributes which can be filled by the user at either creation or editing
  protected $fillable = ['user_id', 'item_id', 'reason', 'status', 'created_at'];
  // These are the headings which can be sorted by the user
  public $sortable = ['status', 'created_at'];
}
