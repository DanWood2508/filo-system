<?php

namespace App\Http\Controllers;
use App\User;
use Gate;

use Illuminate\Http\Request;

class UserController extends Controller
{
  // This will show details about an individual user
  public function show($id) {
    // If the user has permission to view this (is an administrator)
    if (Gate::allows('showUser')) {
      // Find the user and return this and the associated view
      $user = User::find($id);
      return view('user.show', compact('user'));
    }
    // If the user does not have permission to view user details then they will be informed of this
    return back() -> with('failed', 'Only Administrators can view User Details');
  }
}
