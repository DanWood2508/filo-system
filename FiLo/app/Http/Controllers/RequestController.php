<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requests;
use App\Item;
use App\User;
use DB;
use Gate;
use App\ItemDetails;
use App\Mail\RequestMail;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
  // This function will list all requests currently being dealt with
  public function list() {
    // If the user is allowed to see the requests (is an admin)
    if (Gate::allows('listRequests')) {
      // Pass sortable fields to the view and also state that no more than 10 should be on any page
      $requests = Requests::sortable()->paginate(10);
      // Pass all items and users to the view so that these can be used to show relations between tables
      $items = Item::all();
      $users = User::all();
      return view('requests.show', compact('requests', 'items', 'users'));
    }
    return back() -> with('failed', 'Only Administrators can view requests');
  }

  // This function stores a new request when one is made
  public function store(Request $request) {
    // Validation rules, if any of these are violated than an error message will be generated automatically
    $itemRequest = $this -> validate(request(),
    [
      'reason' => 'required',
      'item_id' => 'required',
      'reason'=> 'required',
    ]);
    // Create a new request object and add the form contents to the attribute of the request
    $itemRequest = new Requests;
    $itemRequest->user_id = $request->user()->id;
    $itemRequest->item_id = $request->input('item_id');
    $itemRequest->reason = $request->input('reason');
    $itemRequest->created_at = now();
    // Save this to the table and return a success message to keep the user informed
    $itemRequest->save();
    return back()->with('success', 'Request has been added');
  }

  // This function is designed to update a request following approval or declination
  public function update(Request $request, $id) {
    // If the user is allowed to approve or reject a request (they should be as only admins can press the buttons)
    if (Gate::allows('editRequests')) {
      // Find the request in question
      $req = requests::find($id);
      // Get all requests, items and users to update details
      $requests = Requests::all();
      $items = Item::all();
      $users = User::all();
      // If the request has been approved (from the form which holds the approved button)
      if ($request->input('status') == "Approved") {
        // Cycle through all requests, where the id is equal to the item id then decline these (all other requests declined when one is accepted for the same item)
        foreach ($requests as $currentRequest)
        if($req->item_id == $currentRequest->item_id) {
          DB::table('requests')
          ->where('id', $currentRequest->id)
          ->update([
            'status' => 'Declined',
            'updated_at' => now()
          ]);
        }
      }
      // This will find the specific request in question and set this to the correct status, which is declined if declined is pressed as above the module of code would not be used
      $action = DB::table('requests')
      ->where('id', $id)
      ->update([
        'status' => $request->input('status'),
        'updated_at' => now()
      ]);

      // All requests which could be impacted by this change following the updating of statuses above are selected and iterated over
      $applicableRequests = DB::table('requests')
      ->where('item_id', $req->item_id)
      ->get();

      foreach ($applicableRequests as $thisRequest) {
        // If the request has been actioned (declined or accepted) then this will be used, this stops other requestees being emailed when one request is declined for the same item as they could still be approved
        if($thisRequest->status != "Submitted") {
          // This finds the item and user (attributes of which needed for emails)
          $item = Item::find($thisRequest->item_id);
          $user = User::find($thisRequest->user_id);
          // Send the applicable email message to the users
          Mail::to($user->email)->send(new RequestMail($item, $user, $thisRequest));
        }
      }

      // If the request status was to approve, then this will be returned in a message to inform the user
      if($request->input('status') == "Approved") {
        return back()->with('success', 'Request Approved');
      }
      // If the request status was to decline, then this will be returned in a message to inform the user
      else {
        return back()->with('success', 'Request Declined');
      }
    }
    // If the user does not have permission to accept or decline requests, then this will be returned in an error message
    return back() -> with('failed', 'Only Administrators can approve or reject requests');
  }

  // If a request is deleted, then this will be found and deleted, with a success message being returned to inform the user
  public function destroy($id) {
    if (Gate::allows('deleteRequests')) {
      $request = Requests::find($id);
      $request->delete();
      return back()->with('success', 'Request has been deleted');
    }
    return back()->with('failed', 'Only administrators can delete requests');
  }
}
