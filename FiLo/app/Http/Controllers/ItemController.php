<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use App\Item;
use App\ItemDetails;
use DB;

class ItemController extends Controller
{
  // This function is to list all items and return this on the home view
  public function list() {
    // This gathers all sortable attributes of an item, and ensures that no more than 10 items are on a page
    $items = Item::sortable()->paginate(10);
    // Return the items on the home view
    return view('home',compact('items'));
  }

  // This function shows details of a specific item with id represented by $id
  public function show($id) {
    // This gets the item in question
    $itemQuery = Item::find($id);
    // Get all photos which have an item_id equal to the id of the item, ie all photos for this item
    $photos = DB::table('item_details')
    ->where('item_id', $id)
    ->get();
    // If the user is authorised to show more details of an item then this will be returned
    if (Gate::allows('showDetails')) {
      return view('items.show', compact('itemQuery','photos'));
    }
    // If not, a message will be returned
    return back() -> with('failed', 'Only registered users can view more details');

  }

  // This function is to create an item and add this to the database
  public function create() {
    // If the user is able to create an item (they are a registered user)
    if (Gate::allows('createItem')) {
      // Return the view which holds the form to create an item
      return view('items.create');
    }
    // Return a message to inform the user that they are not permitted to perform this action
    return back() -> with('failed', 'Only registered users can add an item');
  }

  // Store the details of the new item in the database
  public function store(Request $request) {
    // Validation rules for items to be added to the database, will automatically return validation messages
    $item = $this -> validate(request(),
    [
      'category' => 'required',
      'found_time' => 'required',
      'found_date' => 'required',
      'found_place' => 'required',
      'colour' => 'required',
      'photos' => 'required',
      'description' => 'required'
    ]);
    // If photographs have been included and are of the valid format
    if($request->hasFile('photos')) {
      $allowedFileExtension=['jpeg','png','jpg','gif','svg'];
      // Retrieve the files and iterate through these, seperating the name and extension of each
      $files = $request->file('photos');
      foreach($files as $file) {
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        // Ensure that the file uploaded is an allowed type (image formats above)
        $check = in_array($extension, $allowedFileExtension);
      }
      // If the images are approved, then data can be written to the database
      if($check) {
        // Construct a new item object, assign values from the form to the attributes of the item
        $item = new Item;
        $item->category = $request->input('category');
        $item->found_time = $request->input('found_time');
        $item->found_date = $request->input('found_date');
        $item->found_user = $request->user()->id;
        $item->found_place = $request->input('found_place');
        $item->colour = $request->input('colour');
        $item->description = $request->input('description');
        $item->created_at = now();
        // Save this so that there is a valid ID for the photos to reference
        $item->save();
        // If photographs were uploaded then these can be uploaded to the ItemDetails database
        if($request->hasFile('photos')) {
          // for every photo write these to the database and associate them with the correct item
          foreach($request->photos as $photo) {
            $filename = $photo->store('photos');
            $photoID = ItemDetails::create([
              'item_id' => $item->id,
              'filename' => $filename
            ]);
          }
        }
        // Return a success message as the item will have been added
        return back()->with('success', 'Item has been added');
      }
      // Return a message to state that a valid image with the correct extension (format) must be uploaded
      else {
        return back() -> with('failed', 'Please upload a permitted file type (JPEG, PNG, JPG, GIF, SVG)');
      }
    }
  }

  // This function will remove an item and its associated media in the correct order so as not to validate integrity constraints
  public function destroy($id) {
    if (Gate::allows('deleteItem')) {
      // Remove all requests in this items name
      DB::table('requests')
      ->where('item_id', $id)
      ->delete();
      // Remove all media in this items name
      DB::table('item_details')
      ->where('item_id', $id)
      ->delete();
      // Remove the item from the items table
      $item = Item::find($id);
      $item->delete();
      // Return a success message to inform the administrator that this is the case
      return(back()->with('success','Item has been deleted'));
    }
    return back() -> with('failed', 'Only Administrators can delete items');
  }

  // Return the view to edit the item, pass the item in question so that values can be actively edited
  public function edit($id) {
    if (Gate::allows('editItem')) {
      $item = Item::find($id);
      return view('items.edit', compact('item'));
    }
    return back() ->with('failed', 'Only Administrators can edit items');

  }

  // This function will update the item with id equal to $id
  public function update(Request $request, $id) {
    // Find the item in question
    $item = Item::find($id);
    // Validate all fields of the form, all are required as previous values are added to the fields as default
    $this -> validate(request(),[
      'category' => 'required',
      'found_time' => 'required',
      'found_date' => 'required',
      'found_place' => 'required',
      'colour' => 'required',
      'description' => 'required'
    ]);
    // Write the item to the items table
    $item->category = $request->input('category');
    $item->found_time = $request->input('found_time');
    $item->found_date = $request->input('found_date');
    $item->found_user = $request->user()->id;
    $item->found_place = $request->input('found_place');
    $item->colour = $request->input('colour');
    $item->description = $request->input('description');
    $item->updated_at = now();
    // Save this so that the image can reference an existing item so as not to violate any integrity constraints
    $item->save();

    // Validate that the image is uploaded and is of the correct format
    if($request->hasFile('photos')) {
      $allowedFileExtension=['jpeg','png','jpg','gif','svg'];
      $files = $request->file('photos');
      foreach($files as $file) {
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $check = in_array($extension, $allowedFileExtension);
      }
      // If the images uploaded are all acceptable and pass validation
      if($check) {
        // Remove any existing images from the database (better to remove and reupload duplicates than assume consent to keep previous under GDPR)
        if($request->hasFile('photos')) {
          DB::table('item_details')
          ->where('item_id', $item->id)
          ->delete();
          // Write every photo to the database and associate with the relevant item
          foreach($request->photos as $photo) {
            $filename = $photo->store('photos');
            $photoID = ItemDetails::create([
              'item_id' => $item->id,
              'filename' => $filename
            ]);
          }
        }

      }
      // Return a message informing of the file type validation being violated so that the user is aware
      else {
        return back() -> with('failed', 'Please upload a permitted file type (JPEG, PNG, JPG, GIF, SVG)');
      }
    }
    // Return a success message if this has been done successfully so that the user is aware
    return back()->with('success', 'Item has been edited');
  }
}
