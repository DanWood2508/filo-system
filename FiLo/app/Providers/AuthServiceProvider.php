<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Item;

class AuthServiceProvider extends ServiceProvider
{
  /**
  * The policy mappings for the application.
  *
  * @var array
  */
  protected $policies = [
    // 'App\Model' => 'App\Policies\ModelPolicy',§
  ];

  /**
  * Register any authentication / authorization services.
  *
  * @return void
  */
  public function boot()
  {
    $this->registerPolicies();
  }

  public function registerPolicies() {
    // Only registered users can show details of an item
    Gate::define('showDetails', function($user) {
      return $user != null;
    });

    // Only registered users can create an item
    Gate::define('createItem', function($user) {
      return $user != null;
    });

    // Only registered users who are administrators can edit an item
    Gate::define('editItem', function($user) {
      return $user != null && $user->role;
    });

    // Only registered users who are administrators can edit an item
    Gate::define('deleteItem', function($user) {
      return $user != null && $user->role;
    });

    // Only registered users who are administrators can view requests
    Gate::define('listRequests', function($user) {
      return $user != null && $user->role;
    });

    // Only registered users who are administrators can edit requests
    Gate::define('editRequests', function($user) {
      return $user != null && $user->role;
    });

    // Only registered users who are administrators can delete requests
    Gate::define('deleteRequests', function($user) {
      return $user != null && $user->role;
    });

    // Only registered users who are administrators can show user details
    Gate::define('showUser', function($user) {
      return $user != null && $user->role;
    });
  }
}
