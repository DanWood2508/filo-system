<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Item extends Model
{
  use Sortable;
  // Theese are items which can be filled by the user at some point (creation or editing)
  protected $fillable = ['category', 'found_time', 'found_date', 'found_user', 'found_place', 'colour', 'description', 'approved_status', 'created_at'];
  // These are properties which can be sorted in the tables
  public $sortable = ['category','found_date', 'Colour'];
}
