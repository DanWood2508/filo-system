<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDetails extends Model
{
  // These are attributes which can be altered by the user (in item creation or deletion)
  protected $fillable = ['item_id', 'filename'];

  // Determine which this is a "subclass" of
  public function item() {
    return $this->belongsTo('App\Item');
  }
}
