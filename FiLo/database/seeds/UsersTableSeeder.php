<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class UsersTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    // Faker is used to generate realistic and legible data for the table to hold for testing
    $faker = Faker::create();
    $users = User::all()->pluck('id')->toArray();
    // Ten records will be generated
    foreach (range(1,10) as $index) {
      // Numbered from 1 to 10
      DB::table('users')->insert([
        'id' => $index,
        // A random first name will be generated
        'first_name' =>$faker->firstName,
        // A random surname will be generated
        'second_name' =>$faker->lastName,
        // 10 unique emails will be generated
        'email'=>$faker->unique()->email,
        // Random passwords of 12 characters in length will be made
        'password'=>$faker->asciify('************'),
        'created_at' => now()
      ]);
    }
  }
}
