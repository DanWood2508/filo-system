<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Requests;

class RequestsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    // Faker is used to generate realistic and legible data for the table to hold for testing
    $faker = Faker::create();
    $items = Requests::all()->pluck('id')->toArray();
    // Ten records will be generated
    foreach (range(1,10) as $index) {
      DB::table('requests')->insert([
        // Numbered from 1 to 10
        'id' => $index,
        // These will be numbers from 1 to 10 as 10 other users and items are made, thus there will be no integrity constraints violated
        'user_id' => $faker->numberBetween($min = 1, $max = 10),
        'item_id' => $faker->numberBetween($min = 1, $max = 10),
        // A random series of words of maximum length 100 characters (Lorem Ipsum)
        'reason' => $faker->text($maxNbChars = 100),
        'created_at' => now()
      ]);
    }
  }
}
