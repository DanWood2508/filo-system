<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Item;

class ItemsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    // Faker is used to generate realistic and legible data for the table to hold for testing
    $faker = Faker::create();
    $items = Item::all()->pluck('id')->toArray();
    // Ten records will be generated
    foreach (range(1,10) as $index) {
      DB::table('items')->insert([
        // Numbered from 1 to 10
        'id' => $index,
        // A random element from Pet, Phone or Jewellery hence this is the assumption made in the specification
        'category' => $faker->randomElement(["Pet", "Phone", "Jewellery"]),
        // A random time
        'found_time' => $faker->time,
        // A random date
        'found_date' => $faker->date,
        // A random number between 1 and 10 since 10 users are being generated also, this ensures that no integrity constraints are violated
        'found_user' => $faker->numberBetween($min = 1, $max = 10),
        // A random city
        'found_place' => $faker->city,
        // A random colour name
        'colour' => $faker->safeColorName,
        // A random section of text of maximum length 100 characters (Lorem Ipsum)
        'description' => $faker->text($maxNbChars = 100),
        'created_at' => now()
      ]);
    }
  }
}
