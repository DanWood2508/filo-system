<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
  * Seed the application's database.
  *
  * @return void
  */
  public function run()
  {
    // The order in which the database is seeded
    $this->call(UsersTableSeeder::class);
    $this->call(ItemsTableSeeder::class);
    $this->call(RequestsTableSeeder::class);
  }
}
