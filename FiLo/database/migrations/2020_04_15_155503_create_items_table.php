<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    // This is the schema used to create the Items Table
    Schema::create('items', function (Blueprint $table) {
      $table->id();
      $table->string('category');
      $table->time('found_time');
      $table->Date('found_date');
      // The user who found the item
      $table->bigInteger("found_user")->unsigned();
      $table->foreign('found_user')->references("id")->on('users');
      $table->string('found_place');
      $table->string('colour');
      $table->string('description');
      $table->timestamps();
    });
    // Allow foreign keys to be made
    Schema::enableForeignKeyConstraints();
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('items');
  }
}
