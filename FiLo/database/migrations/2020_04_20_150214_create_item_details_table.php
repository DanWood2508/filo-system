<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemDetailsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    // This is the Schema which is used to create the Item_Details table
    Schema::create('item_details', function (Blueprint $table) {
      $table->id();
      // The item details need to be associated with an item so that they can be linked by ID as a foreign key
      $table->bigInteger('item_id')->unsigned();
      $table->foreign('item_id')->references("id")->on('items');
      // The filename (path) to find the image in question
      $table->string('filename');
      $table->timestamps();
    });
    Schema::enableForeignKeyConstraints();
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('item_details');
  }
}
