<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    // This is the Schema which is used to create the Requests Table
    Schema::create('requests', function (Blueprint $table) {
      $table->id();
      // Allow the request to be associated with a user by ID as a foreign key
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      // Allow the request to be associated with an item by ID as a foreign key
      $table->bigInteger('item_id')->unsigned();
      $table->foreign('item_id')->references('id')->on('items');
      $table->text('reason');
      // By default, requests are submitted before being either approved or declined
      $table->string('status')->default('Submitted');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('requests');
  }
}
