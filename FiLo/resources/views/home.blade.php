@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <?php if (Auth::check()) {?>
      <div class="col-md-12">
        <!-- This card will hold a message ot inform the user whether they are logged in or not since the screen can be viewed by non-users and looks similar -->
        <div class="card">
          <div class="card-header">Login Status</div>
          <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
              {{ session('status') }}
            </div>
            @endif
            You are logged in!
          </div>
        </div>
        <br>
      </div>
    <?php } ?>
    <div class="col-md-8">
      <!-- This card will hold a table presenting all unclaimed items -->
      <div class="card">
        <div class="card-header">Items</div>
        <!-- Success and error messages can be showed here from any adjoining forms -->
        @if (\Session::has('success'))
        <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> </div>
          @endif
          @if (\Session::has('failed'))
          <div class="alert alert-warning">
            <p>{{ \Session::get('failed') }}</p> </div>
            @endif
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul> @foreach ($errors->all() as $error)
              <li>{{ $error }}
              </li> @endforeach
            </ul>
          </div>@endif
          <div class="card-body">
            <table border="1" width="100%">
              <thead>
                <tr align="center">
                  <!-- These are sortable and thus can be sorted through by the user for added convenience -->
                  <th>@sortablelink('category')</th>
                  <th>@sortablelink('Colour') </th>
                  <th>@sortablelink('found_date','Found Date') </th>
                  <!-- If the user is registered then they will be able to see this column -->
                  <?php if (Auth::check()) { ?>
                    <th> Action </th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <!-- If there are items to draw (save on unnecessary computation) -->
                @if($items -> count())
                <!-- Iterate through all items -->
                @foreach($items as $key=>$item)
                <!-- Only show items whose requests have not been approved, or have no requests at all, users do not want to see and should not see claimed items -->
                <?php $approvedRequests = DB::table('requests')
                ->where('item_id', $item->id)
                ->where('status', "Approved");
                if (!($approvedRequests -> count())) { ?>
                  <!-- Align all contents centrally to make this more visually appealing -->
                  <tr align="center">
                    <td> {{$item->category }} </td>
                    <td> {{$item->colour }} </td>
                    <!-- Output the date in a user friendly format compared to that used by PHPMyAdmin -->
                    <td> <?php print_r(date('d-m-Y', strtotime($item->found_date))); ?></td>
                    <?php if (Auth::check()) { ?>
                      <td>
                        <!-- Allow details of the items to be viewed by pressing this button -->
                        <a href="{{action('ItemController@show', $item['id'])}}" class="btn btn-primary">Details</a>
                        <!-- If the user is an admionistrator then items can be edited or deleted -->
                        <?php if (Auth::user()->role == 1) {?>
                          <a href="{{action('ItemController@edit', $item['id'])}}" class="btn btn-primary">Edit</a>
                          <form action="{{action('ItemController@destroy', $item['id'])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <!-- Use this hidden field to validate that this button has been clicked -->
                            <input name="_method" type="hidden" value="DELETE" />
                            <button type="submit" class="btn btn-danger">Delete</a>
                            </form>
                          <?php } ?>
                        </td>
                      <?php } ?>
                    </tr>
                  <?php } ?>
                  @endforeach
                  @endif
                </tbody>
              </table>
              <!-- Allow the items in the table to be sorted and paginated -->
              {!! $items->appends(\Request::except('page'))->render() !!}
            </div>
          </div>
        </div>
        <!-- If the user is logged in then they will be able to see additional details and actions in a sidebar, if not a registered user, this will not be drawn-->
        <?php if (Auth::check()) {?>
          <div class="col-md-4">
            <!-- This card will show a summary of the user currently logged in -->
            <div class="card">
              <div class="card-header">User Details</div>
              <div class="card-body">
                <?php $user= Auth::user(); ?>
                <b>First Name: </b> <?php print_r($user->first_name); ?> <br />
                <b>Surname: </b> <?php print_r($user->second_name); ?> <br />
                <b>Email Address: </b> <?php print_r($user->email); ?> <br />
                <b>User Level: </b>
                <?php
                // If the user has a role of 1 then they are an administrator, anything else (0 as boolean) then they are a standard user
                $role = $user->role;
                if ($role == 1) {
                  print_r("Administrator");
                }
                else {
                  print_r("User");
                }
                ?> <br />
                <!-- Format the date to a more user friendly manner than that used in PHPMyAdmin -->
                <b>Joined On: </b> <?php print_r(date_format($user->created_at,"d/m/Y")); ?> <br />
              </div>
            </div>
            <br>
            <div class="card">
              <div class="card-header">Actions</div>
              <div class="card-body">
                <center>
                  <a href="{{action('ItemController@create')}}" class="btn btn-success">Add Items</a>
                  <!-- If the user is an administrator then they can see a button to review requests or view all items rather than just those which are available to request-->
                  <?php if ($role == 1) {?>
                    <a href="{{action('RequestController@list')}}" class="btn btn-danger">Review Requests</a>
                    <a class="btn btn-primary" style="color: white;" data-toggle="modal" data-target="#request">View All Items</a>
                  <?php } ?>
                </center>
                <!-- Open a modal to show the data without the filter to show all items, not just those available for request, this is the same logic as above without the if statement above to select only items available for request -->
                <div class="modal fade" id="request" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Viewing All Items</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">

                        <table border="1" width="100%">
                          <thead>
                            <tr align="center">
                              <th>@sortablelink('category')</th>
                              <th>@sortablelink('Colour') </th>
                              <th>@sortablelink('found_date','Found Date') </th>
                              <?php if (Auth::check()) { ?>
                                <th> Action </th>
                              <?php } ?>
                            </tr>
                          </thead>
                          <tbody>
                            @if($items -> count())
                            @foreach($items as $key=>$item)
                            <tr align="center">
                              <td> {{$item->category }} </td>
                              <td> {{$item->colour }} </td>
                              <td> <?php print_r(date('d-m-Y', strtotime($item->found_date))); ?></td>
                              <!-- Commands for the items claimed can still be accessed as details may have changed or approved items may need to be deleted at the administrators discretion -->
                              <?php if (Auth::check()) { ?>
                                <td>
                                  <a href="{{action('ItemController@show', $item['id'])}}" class="btn btn-primary">Details</a>
                                  <?php if (Auth::user()->role == 1) {?>
                                    <a href="{{action('ItemController@edit', $item['id'])}}" class="btn btn-primary">Edit</a>
                                    <form action="{{action('ItemController@destroy', $item['id'])}}" method="post" enctype="multipart/form-data">
                                      @csrf
                                      <input name="_method" type="hidden" value="DELETE" />
                                      <button type="submit" class="btn btn-danger">Delete</a>
                                      </form>
                                    </td>
                                  <?php } ?>
                                </tr>
                              <?php } ?>
                              @endforeach
                              @endif
                            </tbody>
                          </table>
                          {!! $items->appends(\Request::except('page'))->render() !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <!-- This card will allow the user to see a small card summarising the current status of their requests -->
              <div class="card">
                <div class="card-header">Current Requests</div>
                <div class="card-body">
                  <!-- Access all requests in the request table for the current user (user_id and id of user match) -->
                  <?php $requests = DB::table('requests')
                  ->where('user_id', Auth::user()->id)
                  ->get(); ?>
                  <!-- Present these in an easy-to-read table -->
                  <table border="1" width="100%">
                    <tr>
                      <th>Item Description</th>
                      <th>Current Status</th>
                    </tr>
                    <!-- Cycle through all requests if there are any for the user -->
                    @foreach ($requests as $request)
                    <tr>
                      <td>
                        <!-- Get the item details for the request in question -->
                        <?php $items = DB::table('items')
                        ->where('id', $request->item_id)
                        ->get()?>
                        @foreach ($items as $item)
                        <!-- Present a brief summary of these using simply colour and category -->
                        {{$item->colour}} {{$item->category}}
                        @endforeach
                      </td>
                      <!-- Show the status of this request -->
                      <td>{{$request->status}}</td>
                    </tr>
                    @endforeach
                  </table>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
      @endsection
