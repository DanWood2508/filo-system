@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <!-- This card will hold information about the user -->
      <div class="card">
        <div class="card-header">
          User Details
          <!-- Back to the previous page (requests) -->
          <a type="button" class="close" href ="{{ URL::previous() }}">&times;</a>
        </div>
        @if (\Session::has('success')) <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul> @foreach ($errors->all() as $error)
              <li>{{ $error }}
              </li> @endforeach
            </ul>
          </div>@endif
          <div class="card-body">
            <!-- Output the details of the user from the instance of user passed from the controller -->
            <b>First Name: </b> <?php print_r($user->first_name); ?> <br />
            <b>Surname: </b> <?php print_r($user->second_name); ?> <br />
            <b>Email Address: </b> <?php print_r($user->email); ?> <br />
            <b>User Level: </b>
            <?php
            // If the user has a role rating of 1 then they are an administrator and thus this should be outputted
            $role = $user->role;
            if ($role == 1) {
              print_r("Administrator");
            }
            // If the user has a role rating of anything else (0 as boolean) then the user is a standard user and this should be outputted
            else {
              print_r("User");
            }
            ?> <br />
            <!-- Format the date in a more user friendly manner than that provided by PHPMyAdmin -->
            <b>Joined On: </b> <?php print_r(date_format($user->created_at,"d/m/Y")); ?> <br />
            <br>
            <!-- Back to the previous page (requests) -->
            <a href="{{ URL::previous() }}" class="btn btn-primary">Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
