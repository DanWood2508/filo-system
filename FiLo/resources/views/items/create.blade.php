<!-- inherite master template app.blade.php -->
@extends('layouts.app')
<!-- define the content section --> @section('content')
<div class="container">
  <div class="row justify-content-center">
    <!-- This is for the card to hold elements relating to adding a found item -->
    <div class="col-md-10 "> <div class="card">
      <div class="card-header">Add a Found Item
        <!-- This button is the close button to return back to the ItemController, as this is where it is guaranteed to have been pressed from, and thus saves any redirecting issues -->
        <a type="button" class="close" href ="{{action('ItemController@list')}}">&times;</a>
      </div>
      <!-- display the errors -->
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul> @foreach ($errors->all() as $error)
          <li>{{ $error }}
          </li> @endforeach
        </ul>
      </div><br /> @endif
      <!-- display the success status -->
      @if (\Session::has('success')) <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p> </div><br />
        @endif
        @if (\Session::has('failed'))
        <div class="alert alert-warning">
          <p>{{ \Session::get('failed') }}</p> </div>
          @endif
        <!-- define the form -->
        <div class="card-body">
          <!-- This form will be posting to the table (adding) -->
          <form class="form-horizontal" method="POST" action="{{url('items') }}" enctype="multipart/form-data">
            @csrf
            <div class="col-md-8">
              <!-- Category is a dropdown element -->
              <div class="col-md-8"> <label>Category:</label>
                <select name="category" >
                  <option value="pet">Pet</option>
                  <option value="phone">Phone</option>
                  <option value="jewellery">Jewellery</option>
                </select>
              </div>
              <!-- Time is a time input element with time arrows in Google Chrome and validation included -->
              <div class="col-md-8">
                <label >Time Found:</label>
                <input type="time" name="found_time" placeholder="HH:MM" />
              </div>
              <!-- Date is a date element with a calendar in some browsers and validation included -->
              <div class="col-md-8">
                <label >Date Found:</label>
                <input type="date" name="found_date" placeholder="DD/MM/YY" />
              </div>
              <div class="col-md-8">
                <label >Place Found:</label>
                <input type="text" name="found_place" placeholder="Brief Description" />
              </div>
              <div class="col-md-8">
                <label>Colour:</label>
                <input type="text" name="colour" placeholder="Colour" />
              </div>
              <!-- This is expecting to take an array, this could be one or more images -->
              <div class="col-md-8">
                <label>Image: (JPEG, PNG, JPG, GIF, SVG)</label>
                <input type="file" name="photos[]" placeholder="Image File" multiple/>
              </div>
              <div class="col-md-8">
                <label>Description:</label>
                <textarea rows="4" cols="50" name="description" placeholder="Brief Description of the Item"></textarea>
              </div>
              <!-- Buttons to either submit or reset the form as default for added consistency -->
              <div class="col-md-6 col-md-offset-4">
                <input type="submit" class="btn btn-primary" />
                <input type="reset" class="btn btn-primary" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
