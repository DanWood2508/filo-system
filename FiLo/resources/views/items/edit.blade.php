@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-14">
      <div class="card">
        <!-- This card holds the form to edit the items in the Items table -->
        <div class="card-header">
          Edit Item - {{$item->colour}} {{$item->category}}
          <!-- This button allows the window to be closed and return to a listing of items since most users will want to ensure that their item has been edited -->
          <a type="button" class="close" href ="{{action('ItemController@list')}}">&times;</a>
        </div>
        @if (\Session::has('success')) <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul> @foreach ($errors->all() as $error)
              <li>{{ $error }}
              </li> @endforeach
            </ul>
          </div>@endif
          @if (\Session::has('failed'))
        	<div class="alert alert-warning">
                  <p>{{ \Session::get('failed') }}</p> </div>
                  @endif
          <div class="card-body">

<!-- This form has method post as data will be added / altered, there is a submethod of patch to specify that editing will be occuring here -->
<form class="form-horizontal" method="POST" action="{{action('ItemController@update', $item['id'])}}" enctype="multipart/form-data">
  @METHOD('PATCH')
  @csrf
  <div class="col-md-8">
    <label>Category:</label>
    <!-- Since it is not possible without jQuery to dynamically set the default option, a clear previous selected box is added so the user is aware of the reason there is duplication
    This must be kept as otherwise the user may unintentionally alter the category of an item assuming this is unchanged -->
    <select name="category" >
      <option value="{{$item->category}}">Previously Selected: {{$item->category}}</option>
      <option value="Pet">Pet</option>
      <option value="Phone">Phone</option>
      <option value="Jewellery">Jewellery</option>
    </select>
  </div>
  <!-- The value field is being used here as this writes the existing value to the input fields and thus data does not have to be changed or remembered if not needed -->
  <div class="col-md-8">
    <label >Time Found:</label>
    <input type="time" name="found_time" placeholder="HH:MM" value="{{$item->found_time}}"/>
  </div>
  <div class="col-md-8">
    <label >Date Found:</label>
    <input type="date" name="found_date" placeholder="DD/MM/YY" value="{{$item->found_date}}" />
  </div>
  <div class="col-md-8">
    <label >Place Found:</label>
    <input type="text" name="found_place" placeholder="Brief Description"  value="{{$item->found_place}}"/>
  </div>
  <div class="col-md-8">
    <label>Colour:</label>
    <input type="text" name="colour" placeholder="Colour" value="{{$item->colour}}" />
  </div>
  <!-- Expecting at least one or more images, however this is not required in this instance, meaning existing images will be left existing -->
  <div class="col-md-8">
    <label>Image: (JPEG, PNG, JPG, GIF, SVG)</label>
    <input type="file" name="photos[]" placeholder="Image File" multiple/>
  </div>
  <div class="col-md-8">
    <label>Description:</label>
    <textarea rows="4" cols="50" name="description" placeholder="Brief Description of the Item">{{$item->description}}</textarea>
  </div>
  <!-- Submit and Reset buttons as standard -->
  <div class="col-md-6 col-md-offset-4">
    <input type="submit" class="btn btn-primary" />
    <input type="reset" class="btn btn-primary" />
  </div>
  <br>
</form>
</div>
</div>
</div>
</div>
</div>
@endsection
