<!-- inherite master template app.blade.php -->
@extends('layouts.app')
<!-- define the content section --> @section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 ">
      <!-- This will contain all content relating to showing details of the item -->
      <div class="card">
        <div class="card-header">Full Item Details - {{$itemQuery->colour}} {{$itemQuery->category}}
      </div>
        <!-- display the success status -->
        @if (\Session::has('success')) <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul> @foreach ($errors->all() as $error)
              <li>{{ $error }}
              </li> @endforeach
            </ul>
          </div>@endif
          @if (\Session::has('failed'))
          <div class="alert alert-warning">
            <p>{{ \Session::get('failed') }}</p> </div>
            @endif
          <div class="card-body">
            <!-- Present the data in a class for ease of visibility -->
            <table class = "table table-striped" border="2">
              <!-- This allows CSS Code and variables (with attributes) to be referenced and placed -->
              <tr> <th> Category </th> <td> {{$itemQuery->category}}</td></tr>
              <tr> <th>Time Found </th> <td>{{$itemQuery->found_time}}</td></tr>
              <tr> <th>Date Found </th> <td>{{$itemQuery->found_date}}</td></tr>
              <tr> <th>Place Found </th> <td>{{$itemQuery->found_place}}</td></tr>
              <tr> <th>User Found </th> <td>
                <!-- Search the user table for the user for the user with the matching id -->
                <?php $users = DB::table('users')
                    ->where('id', $itemQuery->found_user)
                    ->get(); ?>
                <!-- This outputs the data as an array and thus must be cycled through, the details of the user are outputted to the table -->
                @foreach($users as $user)
                {{$user->first_name}} {{$user->second_name}} @endforeach</td> </tr>
              <tr> <th>Color </th> <td>{{$itemQuery->colour}}</td></tr>
              <tr> <th>Photo </th> <td>
                <!-- Images are displayed in a carousel -->
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    @foreach( $photos as $photo )
                    <?php if($photo->item_id == $itemQuery->id) { ?>
                      <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    <?php } ?>
                    @endforeach
                  </ol>

                  <div class="carousel-inner" role="listbox">
                    @foreach( $photos as $photo )
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                      <img class="d-block img-fluid" src="{{asset('../storage/app/'.$photo->filename)}}">
                    </div>
                    @endforeach
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>

              </td></tr>
              <tr> <th>Description </th> <td>{{$itemQuery->description}}</td></tr>
            </table>
            <a href="{{action('ItemController@list')}}" class="btn btn-primary">Back to Items</a>
            <!-- If the user is an administrator then they can return to the request screen if this is where they came from -->
            <?php if (Auth::user()->role == 1) {?><a href="{{action('RequestController@list')}}" class="btn btn-primary">Back To Requests</a> <a href="{{action('ItemController@edit', $itemQuery['id'])}}" class="btn btn-primary">Edit</a> <?php }
            // Iterate through all requests relevant to this item
            $requests = DB::table('requests')
              ->where('item_id', $itemQuery->id)
              ->get('status');
            // If there are requests for this item then count how many are approved
            if (sizeof($requests) > 0) {
              $approvedCount = 0;
              foreach($requests as $request) {
                if ($request->status == "Approved") {
                  $approvedCount++;
                }
              }
              // If at no requests for this item is approved, then this can be requested
              if ($approvedCount < 1) { ?>
                <a class="btn btn-success" style="color: white;" data-toggle="modal" data-target="#request">Request</a>
              <?php }
            }
            // If there are no requests in the items name, then allow this to be requested - never two request buttons drawn in this instance
            else { ?>
              <a class="btn btn-success" style="color: white;" data-toggle="modal" data-target="#request">Request</a>
            <?php } ?>

            <!-- If the request button is pressed, then a modal window is opened to save the amount of page redirecting -->
            <div class="modal fade" id="request" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Request {{$itemQuery->colour}} {{$itemQuery->category}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <!-- A form with the post method to ensure that data can be added to the request table -->
                    <form class="form-horizontal" method="POST" action="{{url('requests') }}" enctype="multipart/form-data">
                      @csrf
                      <div class="col-md-8">
                        <div class="col-md-8">
                          <label >Reason:</label>
                          <textarea rows="4" cols="50" name="reason" placeholder="Brief Reason for requesting the Item"/></textarea>
                        </div>
                        <input type="hidden" name="item_id" value="{{$itemQuery->id}}" />
                        <!-- There is no need for a request button in this instance as only one field is taken -->
                        <div class="col-md-6 col-md-offset-4">
                          <input type="submit" class="btn btn-primary" />
                        </div>
                        <br>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    @endsection
