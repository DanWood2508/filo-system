@component('mail::message')
# Your request has been {{ $status }}!

Hello {{ $firstName }} {{ $secondName }},

This email is to confirm that your request for a {{ $itemColour }} {{ $itemCategory }} has been {{ $status }}. Your request was submitted at {{ $requestSubmitted }}.

For more details please visit the FiLo System.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
