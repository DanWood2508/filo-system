<!DOCTYPE html>
<!-- Whilst this page may not be necessary, it is ideal as this provides a landing page rather than being greeted with data immediately, promotional material could be added here also in the future -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>FiLo</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

  <!-- Styles -->
  <style>
  html, body {
    background-color: #fff;
    color: #636b6f;
    font-family: 'Nunito', sans-serif;
    font-weight: 200;
    height: 100vh;
    margin: 0;
  }

  .full-height {
    height: 100vh;
  }

  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .position-ref {
    position: relative;
  }

  .top-right {
    position: absolute;
    right: 10px;
    top: 18px;
  }

  .content {
    text-align: center;
  }

  .title {
    font-size: 84px;
  }

  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
  }

  .m-b-md {
    margin-bottom: 30px;
  }
  </style>
</head>
<body>
  <!-- Header area for the main page -->
  <div class="flex-center position-ref full-height">
    <div class="top-right links">
      <!--If the user is logged in then they can see all items (the view itself will filter content away that is not permitted) -->
      @if (Route::has('login'))
      <a href="{{ url('/home') }}">Items</a>
      @auth
      @else
      <!-- If the user is not logged in they can either login or register -->
      <a href="{{ route('login') }}">Login</a>
      @if (Route::has('register'))
      <a href="{{ route('register') }}">Register</a>
      @endif
      @endauth
      @endif
      <!--If this is a valid user then they can add an item -->
      @if (Auth::user())
      <a class="nav-link" href="{{action('ItemController@create') }}">Add Item</a>
      <!-- If this is a valid user and administrator then they can review requests -->
      @if (Auth::user()->role == 1)
      <a class="nav-link" href="{{action('RequestController@list') }}">View Requests</a>
      @endif
      @endif
    </div>
    <div class="content">
      @if (\Session::has('failed'))
      <div class="alert alert-warning">
        <p style="color:red;">{{ \Session::get('failed') }}</p> </div>
        @endif
      <div class="title m-b-md">
        <!-- Title of the page -->
        FiLo System
      </div>

      <div class="links">
        <!-- Small tagline for the system to add identity -->
        <p>Lost and Found System</p>
      </div>
    </div>
  </div>
</body>
</html>
