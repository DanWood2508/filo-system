@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-14">
      <!-- This card will hold a table of requests -->
      <div class="card">
        <div class="card-header">Requests</div>
        @if (\Session::has('success')) <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul> @foreach ($errors->all() as $error)
              <li>{{ $error }}
              </li> @endforeach
            </ul>
          </div>@endif
          <div class="card-body">
            <!--Present the details of the requests in a table for clarity -->
            <table border="1">
              <thead>
                <tr align="center">
                  <th> Item Category </th>
                  <th> Colour </th>
                  <th> Date / Time Found </th>
                  <th> More Item Details </th>
                  <th> Requested Name </th>
                  <th> More User Details </th>
                  <th> Reason </th>
                  <!-- In this instance the date / time submitted and status are sortable for ease of use for the administrator -->
                  <th> @sortablelink('created_at','Submitted At') </th>
                  <th> @sortablelink('status','Status') </th>
                  <!-- No need to filter this as the table will only ever be reviewed by administrators -->
                  <th> Action </th>
                </tr>
              </thead>
              <tbody>
                <!-- For every controller generate buttons to view more details on the item in question and the requesting user -->
                @foreach($requests as $request)
                <tr>
                  <!-- For each item, if this is relevant to the request in question, add a link to its details page -->
                  @foreach($items as $item)
                  <?php if($item->id == $request->item_id) { ?>
                    <td> {{$item->category }}</td>
                    <td> {{$item->colour }}</td>
                    <td> {{$item->found_date }} {{$item->found_time }} </td>
                    <td> <a href="{{action('ItemController@show', $item['id'])}}" class="btn btn-primary">Details</a> </td>
                  <?php } ?>
                  @endforeach

                  <!-- For each user, if this is relevant to the request in question, add a link to their details page -->
                  @foreach($users as $user)
                  <?php if($user->id == $request->user_id) { ?>
                    <td> {{$user->first_name }} {{$user->second_name}} </td>
                  <?php } ?>
                  @endforeach
                  <td><a href="{{action('UserController@show', $request->user_id)}}" class="btn btn-primary">Details</a></td>
                  <td>{{$request->reason}}</td>
                  <td>{{$request->created_at}}</td>
                  <td>{{$request->status}}</td>

                  <td>
                    <!-- If the request has been submitted, then the approve or decline buttons will be visible -->
                    <?php if($request->status == "Submitted") { ?>
                      <form action="{{action('RequestController@update', $request['id'])}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <!-- This is a hidden input to show the controller which option has been chosen, approve or request -->
                        <input type="hidden" name="status" value="Approved"/>
                        <button class="btn btn-success" type="submit"> Accept</button>
                      </form>
                      <form action="{{action('RequestController@update', $request['id'])}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <!-- This is a hidden input to show the controller which option has been chosen, approve or request -->
                        <input type="hidden" name="status" value="Declined"/>
                        <button class="btn btn-danger" type="submit"> Decline</button>
                      </form>
                      <!-- If the request is not submitted then it is either approved or declined, and thus can be deleted if required in the controller -->
                    <?php } else { ?>
                      <form action="{{action('RequestController@destroy', $request['id'])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <!-- This is a hidden input to state that the item has been chosen to be deleted for the controller-->
                        <input name="_method" type="hidden" value="DELETE" />
                        <button type="submit" class="btn btn-danger">Delete</a>
                        </form>
                      <?php } ?>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <!-- Allow the table to be sorted through and to be paginated -->
                {!! $requests->appends(\Request::except('page'))->render() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection
